INTRODUCTION
------------

The Panopoly Base Distribution Starter Kit provides an entry point for
developers to create new distributions utilizing Panopoly as a base
distribution.

@see http://drupal.org/project/panopoly
@see http://drupal.org/documentation/build/distributions

Several variations on the starter kit are available. The differences between
these variations lie in which version of Panopoly is used and whether or not
patches are included in the make files.

* RELEASE (default)
  Uses the latest recommended Panopoly release.

* DEVELOPMENT
  Uses the latest development branch.


INSTALLATION
------------

This project provides a drush command that needs to be accessible by drush. An
ideal place to add this project folder is ~/.drush which is the default
behavior.

  $ drush dl panopoly_base_distribution_starter_kit


CREATING THE DISTRIBUTION
-------------------------

* AUTOMATIC (Using Drush)

  $ drush panopoly-starter-kit [name]

  Arguments:
    name                  A name for the new distribution.
    machine_name          [optional] A machine-readable name for the new
                            distribution.

  Options:
    --name
      A name for the new distribution.

    --machine-name
      [optional] A machine-readable name for the new distribution.

    --description
      [optional] A description for the new distribution.

    --dev
      [optional] Use the development release of Panopoly. Default is to use
      recommended release.

    --include-theme-selection
      [optional] Use the Panopoly theme selection during the install process.

    --include-apps-server
      [optional] Include the Panopoly Apps server.

    --recommended-modules
      [optional] Include recommended modules in .info file. Defaults to not
      included (commented out).

    --overwrite
      [optional] Overwrite the destination *files* if they already exists.
      Defaults to not overwrite.

    --path
      [optional] The path where the new distribution will be created. Defaults
      to [current_directory]/[machine_name].

    --local
      [optional] Writes the .make files to work with the make_local project.

  Aliases: psk

  Examples:
    $ drush psk "My Distribution Name"
    Create a distribution using the default options.

    $ drush psk "My Distribution Name" my_distro
    Create a distribution with a specific machine name.

    $ drush psk "My Distribution Name" --path=~/projects/ --overwrite
    Create a new distribution at ~/projects/[new_machine_name].
    Overwrite the destination if it already exists.

    $ drush psk "My Distribution Name" --dev
    Create a distribution using the Panopoly development release.

    $ drush psk "My Distribution Name" --include-patches
    Create a distribution that includes recommended patches.


VERIFICATION
------------

After completing the automatic or manual process noted above, the new folder and
file contents should look like.

/my_new_distribution (the new folder)
-- build-my-new-distribution.make
-- drupal-org-core.make
-- drupal-org.make
-- my_new_distribution.info
-- my_new_distribution.install
-- my_new_distribution.profile


RECOMMENDED MODULES
-------------------

The STARTERKIT.info (e.g. my_new_distribution.info) file includes a list of
recommended modules commented out.  To include these modules in the new
distribution simply uncomment those lines in the .info file.

These modules include:

; Panopoly Recommended - Admin
;dependencies[] = backports
;dependencies[] = breakpoints
;dependencies[] = date_popup_authored
;dependencies[] = module_filter
;dependencies[] = navbar
;dependencies[] = save_draft
;dependencies[] = simplified_menu_admin
;dependencies[] = views_ui

; Panopoly Recommended - Other
;dependencies[] = apps
;dependencies[] = defaultcontent
;dependencies[] = devel
;dependencies[] = uuid

APP SERVER
----------

@todo
