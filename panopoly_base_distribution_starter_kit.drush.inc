<?php
/**
 * @file
 * Contains functions only needed for drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function panopoly_base_distribution_starter_kit_drush_command() {
  $items = array();

  $items['panopoly-starter-kit'] = array(
    'description' => 'Create a distribution utilizing Panopoly as a base distribution.',
    'arguments' => array(
      'name' => 'A name for the new distribution.',
      'machine_name' => '[optional] A machine-readable name for the new distribution.',
    ),
    'options' => array(
      'name' => 'A name for the new distribution.',
      'machine-name' => '[optional] A machine-readable name for the new distribution.',
      'path' => '[optional] The path where the new distribution will be created. Defaults to [current_directory]/[machine_name].',
      'overwrite' => '[optional] Overwrite the destination *files* if they already exists. Defaults to not overwrite.',
      'description' => '[optional] A description for the new distribution.',
      'dev' => '[optional] Use the development release of Panopoly. Default is to use recommended release.',
      'recommended-modules' => '[optional] Include recommended modules in .info file. Defaults to not included (commented out).',
      'include-apps-server' => '[optional] Include the install task in the .profile for the Panopoly Apps Server.',
      'include-theme-selection' => '[optional] Include the Panopoly theme selection during the install process.',
      'local' => '[optional] Reference the new distribution folder in the build make file. REQUIRES the drush make_local module.',
    ),
    'examples' => array(
      'drush panopoly-starter-kit "My Distribution Name"' => 'Create a distribution using the default options.',
      'drush psk "My Distribution Name" my_distro --description="My New Distro"' => 'Create a distribution with a specific machine name and description.',
      'drush psk "My Distribution Name" --path=/home/caschbre/projects --overwrite' => 'Create a new distribution at ~/projects/[new_machine_name]. Overwrite the destination if it already exists.',
      'drush psk "My Distribution Name" --dev' => 'Create a distribution using the Panopoly development release.',
      'drush psk "My Distribution Name" --recommended-modules' => 'Create a distribution that installs recommended contrib modules.',
      'drush psk "My Distribution Name" --include-apps-server' => 'Create a distribution that installs the Panopoly Apps Server.',
    ),
    'aliases' => array('psk'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function panopoly_base_distribution_starter_kit_drush_help($section) {
  switch ($section) {
    case 'drush:panopoly-starter-kit':
      return dt('The panopoly-starter-kit (psk) command creates a distribution using the Panopoly Base Distribution Starter kit. See README.txt for more information.');
  }
}

/**
 * Create a Panopoly-based distribution using the starter kit.
 */
function drush_panopoly_base_distribution_starter_kit_panopoly_starter_kit($name = NULL, $machine_name = NULL) {
  // Determine the distribution name.
  if (!isset($name)) {
    $name = drush_get_option('name');
  }

  // Determine the machine name.
  if (!isset($machine_name)) {
    $machine_name = drush_get_option('machine-name');
  }
  if (!$machine_name) {
    $machine_name = $name;
  }
  $machine_name = str_replace(' ', '', strtolower($machine_name));
  $search = array(
    '/[^a-z0-9_]/', // Remove characters not valid in function names.
    '/^[^a-z]+/',   // Functions must begin with an alpha character.
  );
  $machine_name = preg_replace($search, '', $machine_name);

  // Determine the description.
  $description = dt('!name distribution based on the Panopoly distribution.', array(
    '!name' => $name,
  ));
  if ($description_arg = drush_get_option('description')) {
    $description = dt('!description', array(
      '!description' => $description_arg,
    ));
  }

  // Determine the path to the new distribution.
  $distro_path = drush_normalize_path(getcwd() . '/' . $machine_name);
  if ($path = drush_get_option('path')) {
    $distro_path = drush_normalize_path(drush_trim_path($path) . '/' . $machine_name);
  }

  // Make a fresh copy of the original starter kit.
  // $starter_path = drush_normalize_path(drush_get_option('include') . '/' . 'STARTERKIT');
  $starter_path = drush_normalize_path(dirname(__FILE__) . '/STARTERKIT');
  if (!is_dir(dirname($starter_path))) {
    drush_die(dt('The directory "!directory" was not found.', array('!directory' => dirname($distro_path))));
  }
  $overwrite_action = drush_get_option('overwrite') ? FILE_EXISTS_OVERWRITE : FILE_EXISTS_ABORT;
  if (!drush_op('drush_copy_dir', $starter_path, $distro_path, $overwrite_action)) {
    drush_die(dt('Unable to copy the STARTERKIT to !path. Try using the --overwrite option or sudo.', array(
      '!path' => $distro_path,
    )));
  }

  // Set panopoly version to 1.x if 'dev' option passed.
  if (drush_get_option('dev')) {
    $panopoly_version = '1.x';
  }
  // Attempt to get latest panopoly version.
  // Note: parse_ini_file and parse_ini_string were unable to handle multi-
  // dimensional arrays used in .make files.
  else {
    // Get the contents of the build-panopoly.make file and explode each line
    // into an array element. Locate the line with the version number and then
    // parse_str to turn it into an array that we can grab the version.
    $panopoly_build_make_string = file_get_contents("http://cgit.drupalcode.org/panopoly/plain/build-panopoly.make");
    $panopoly_build_make_array = explode("\n", $panopoly_build_make_string);
    foreach ($panopoly_build_make_array as $string) {
      if (strpos($string, 'projects[panopoly][version]') !== FALSE) {
        $version_array = array();
        parse_str($string, $version_array);
        $panopoly_version = trim($version_array['projects']['panopoly']['version']);
        break;
      }
    }
  }

  if (!isset($panopoly_version)) {
    drush_die(dt('Unable to determine the latest Panopoly version. Use the --dev option to grab the latest dev version.'));
  }

  // Grab a copy of Panopoly's drupal-org[dev/release].make file.
  foreach (array('drupal-org-dev.make', 'drupal-org-release.make') as $do_make_filename) {
    $do_make_string = file_get_contents("http://cgit.drupalcode.org/panopoly/plain/{$do_make_filename}");
    $do_make_array = explode("\n", $do_make_string);
    // Insert some helper text / comments into the new drupal-org.make file.
    foreach ($do_make_array as $delta => $line) {
      // Insert after 'core = '
      if (strpos($line, 'core') !== FALSE) {
        $insertpoint = $delta + 1;
        break;
      }
    }

    // The actual inserting/appending of comments.
    $insert_text = file_get_contents(drush_normalize_path(dirname(__FILE__) . '/assets/drupal-org-make-insert.inc'));
    $insert_text_array = explode("\n", $insert_text);
    array_splice($do_make_array, $insertpoint, 0, $insert_text_array);
    $do_make_string = implode("\n", $do_make_array);
    $do_make_string .= file_get_contents(drush_normalize_path(dirname(__FILE__) . '/assets/drupal-org-make-append.inc'));
    file_put_contents(drush_normalize_path($distro_path . "/{$do_make_filename}"), $do_make_string);

    // Also make the drupal-org.make file based on the --dev option.
    if (drush_get_option('dev') && $do_make_filename == 'drupal-org-dev.make') {
      file_put_contents(drush_normalize_path($distro_path . "/drupal-org.make"), $do_make_string);
    }
    elseif (!drush_get_option('dev') && $do_make_filename == 'drupal-org-release.make') {
      file_put_contents(drush_normalize_path($distro_path . "/drupal-org.make"), $do_make_string);
    }
  }

  // Alter the contents of the new files.
  $replacements = array(
    '[STARTERKIT_NAME]' => $name,
    '[STARTERKIT_MACHINE_NAME]' => $machine_name,
    '[STARTERKIT-MACHINE-NAME]' => str_replace('_', '-', $machine_name),
    '[STARTERKIT_DESCRIPTION]' => $description,
    ';[STARTERKIT_RECOMMENDED]' => drush_get_option('recommended-modules') ? '' : ';',
    '//[STARTERKIT_APPS]' => drush_get_option('include-apps-server') ? '' : '//',
    ';[STARTERKIT_APPS]' => drush_get_option('include-apps-server') ? '' : ';',
    ';[STARTERKIT_LOCAL]' => drush_get_option('local') ? '' : ';',
    ';[STARTERKIT_GIT]' => drush_get_option('local') ? ';' : '',
    '[STARTERKIT_DISTRO_PATH]' => drush_normalize_path($distro_path),
    '[STARTERKIT_PANOPOLY_VERSION]' => $panopoly_version,
    '//[STARTERKIT_THEME_SELECTION]' => drush_get_option('include-theme-selection') ? '' : '//',
  );

  $file_pattern = drush_normalize_path($distro_path . '/*');
  $files = glob($file_pattern, GLOB_ERR);
  foreach ($files as $file) {
    drush_op('panopoly_base_distribution_starter_kit_file_str_replace', $file, array_keys($replacements), array_values($replacements));
  }

  // Loop through each file and alter the filename.
  $file_pattern = drush_normalize_path($distro_path . '/*.starterkit');
  $files = glob($file_pattern, GLOB_ERR);
  foreach ($files as $file) {
    // Remove the .starterkit extention
    $new_file = str_replace('.starterkit', '', $file);

    // build-STARTERKIT.make uses hyphens instead of underscores so we need to
    // check for that file and make the appropriate change to the machine_name.
    if (strstr($new_file, 'build-')) {
      $new_file = str_replace('STARTERKIT', str_replace('_', '-', $machine_name), $new_file);
    }
    else {
      $new_file = str_replace('STARTERKIT', $machine_name, $new_file);
    }

    drush_op('rename', $file, $new_file);
  }

  // Get a copy of Panopoly's drupal-org-core.make file.
  $do_core_make = "http://cgit.drupalcode.org/panopoly/plain/drupal-org-core.make";
  if (drush_get_option('dev')) {
    $do_core_make .= "?id=7.x-{$panopoly_version}";
  }

  file_put_contents(
    drush_normalize_path($distro_path . '/drupal-org-core.make'),
    file_get_contents($do_core_make)
  );

  // Notify user of the newly created distribution.
  drush_print(dt('Congrats! Starter kit for "!name" created in !path using the !make make file.', array(
    '!name' => $name,
    '!path' => $distro_path,
    '!make' => $do_make_file,
  )));
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_panopoly_base_distribution_starter_kit_panopoly_starter_kit_validate($name) {
  if (empty($name)) {
    return drush_set_error('MISSING_NAME', dt('Please provide a Name for the new distribution.'));
  }
}

/**
 * Replace strings in a file.
 */
function panopoly_base_distribution_starter_kit_file_str_replace($file_path, $find, $replace) {
  $file_path = drush_normalize_path($file_path);
  $file_contents = file_get_contents($file_path);
  $file_contents = str_replace($find, $replace, $file_contents);
  file_put_contents($file_path, $file_contents);
}
